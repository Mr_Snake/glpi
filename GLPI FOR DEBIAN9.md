Mise en place serveur Web (LAMP)

Avant d’installer GLPI, il nous faut bien sûr un serveur Web et nous allons mettre en place le serveur web LAMP pour Linux Apache MySQL (ou MariaDB) PHP
Installation Apache2

    root@pixelabs:~# apt install apache2

Vérifiez :

    root@pixelabs:~# systemctl status apache2

Installation Apache2 Debian

Vous pouvez aussi vérifier via : http://localhost/
Installation PHP 7.0

Et si on installe PHP 7 :

    root@pixelabs:~# apt install php7.0 php7.0-curl php7.0-json php7.0-gmp php7.0-mbstring php7.0-gd php7.0-mcrypt libapache2-mod-php7.0 php7.0-mysql php7.0-intl php7.0-sqlite3 php7.0-xml php7.0-zip

    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances       
    Lecture des informations d'état... Fait
    The following additional packages will be installed:
      libcurl3 libmcrypt4 libzip4 php-common php7.0-cli php7.0-common php7.0-opcache php7.0-readline psmisc
    Paquets suggérés :
      php-pear libmcrypt-dev mcrypt
    Les NOUVEAUX paquets suivants seront installés :
      libapache2-mod-php7.0 libcurl3 libmcrypt4 libzip4 php-common php7.0 php7.0-cli php7.0-common php7.0-curl php7.0-gd php7.0-gmp php7.0-intl php7.0-json
      php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-opcache php7.0-readline php7.0-sqlite3 php7.0-xml php7.0-zip psmisc
    0 mis à jour, 22 nouvellement installés, 0 à enlever et 127 non mis à jour.
    Il est nécessaire de prendre 5 044 ko dans les archives.
    Après cette opération, 19,2 Mo d'espace disque supplémentaires seront utilisés.
    Souhaitez-vous continuer ? [O/n] 

Confirmer et attendez la fin. Une fois terminé, relancez Apache :

    root@pixelabs:~# systemctl restart apache2

Pour tester le PHP, créez un fichier pixelabs.php dans le répertoire /var/www/html

    root@pixelabs:~# cd /var/www/html
    root@pixelabs:/var/www/html# 
    root@pixelabs:/var/www/html# nano pixelabs.php

Ajoutez-y :

    <?php 
    phpinfo(); 
    ?>

Ouvrir ce fichier via votre navigateur : http://localhost/pixelabs.php

Installation PHP 7 Debian 9
Installation MariaDB 10.1

Installation de MariaDB version 10 :

    root@pixelabs:~# apt install mariadb-server-10.1

Pour sécuriser MariaDB, à la racine de votre serveur (cd puis entrée) exécutez le script suivant :

    root@pixelabs:~# mysql_secure_installation
    ..
    ..
    Enter current password for root (enter for none): // Entrée (pas de password par défaut)
    ...
    Set root password? [Y/n] // si vous souhaitez ajouter un mot de passe ou pas.
    ...
    Remove anonymous users? [Y/n] Y
    Disallow root login remotely? [Y/n] Y
    Remove test database and access to it? [Y/n] Y
    Reload privilege tables now? [Y/n] Y

Il est possible de relancer le script.
Base de données GLPI

Nous allons créer une base de données pour GLPI. Connectez-vous à la base MariaDB :

    root@pixelabs:~# mysql -u root
    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 5
    Server version: 10.1.37-MariaDB-0+deb9u1 Debian 9.6
    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.
    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    MariaDB [(none)]>

Création de la base : glpi_pixelabs

    MariaDB [(none)]#> CREATE DATABASE glpi_pixelabs;
    Query OK, 1 row affected (0.00 sec)
    MariaDB [(none)]>

Création de l’user glpi avec le mot de passe glpi :

    MariaDB [(none)]#> CREATE USER 'glpi'@'localhost' IDENTIFIED BY 'glpi';
    Query OK, 0 rows affected (0.00 sec)
    MariaDB [(none)]> 

Droit d’écriture/lecture à l’user glpi sur la base glpi_pixelabs :

    MariaDB [(none)]#> GRANT ALL PRIVILEGES ON glpi_pixelabs.* TO 'glpi'@'localhost' WITH GRANT OPTION;
    Query OK, 0 rows affected (0.00 sec)
    MariaDB [(none)]> exit

C’est terminé.
Installation & Configuration de GLPI v.9.3.3

Passons maintenant à l’installation de GLPI.
Installation

Télécharger l’archive dans /tmp

    root@pixelabs:~# cd /tmp
    root@pixelabs:/tmp# wget https://github.com/glpi-project/glpi/releases/download/9.3.3/glpi-9.3.3.tgz

Dézipper l’archive .tgz :

    root@pixelabs:/tmp# tar zxvf glpi-9.3.3.tgz

Une fois terminé, vous aurez un dossier glpi qu’il faut déplacer dans /var/www/

    root@pixelabs:/tmp# cp -r glpi /var/www/
    root@pixelabs:/tmp# cd /var/www/
    root@pixelabs:/var/www# ls -l
    total 8
    drwxr-xr-x 18 root root 4096 déc.  17 21:53 glpi
    drwxr-xr-x  2 root root 4096 déc.  17 20:39 html
    root@pixelabs:/var/www#

Donner les droits à Apache2 :

    root@pixelabs:/var/www# chown -R www-data /var/www/glpi

Et pour finir, nous allons modifier le fichier Apache afin de le pointer à la racine de /var/www au lieu de /var/www/html

    root@pixelabs:/var/www# nano /etc/apache2/sites-available/000-default.conf

Modifier la ligne :

    DocumentRoot /var/www/html

En :

    DocumentRoot /var/www/

Sauvegarder, quitter et relancer Apache2 :

    root@pixelabs:/var/www# systemctl restart apache2